import { useState, useEffect, useMemo } from "react";
import { Container } from "./components/Container";
import { Layout } from "./components/Layout";
import { List } from "./components/List";
import { ListItem } from "./components/ListItem";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { ThemeProvider } from "./components/ThemeProvider";
import fetchApi, { METHODS } from "./helpers/api";
import { Form } from "./components/form";

interface Item {
    id: number;
    title: string;
    done: boolean;
    createdAt: number;
    finishedAt: number;
}


const sortItems = (items: Item[]) => {
    return items.sort((a, b) => {
        if (a.done === b.done) {
            return b.createdAt - a.createdAt;
        }
        return a.done ? 1 : -1;
    });
};
  

export const App: React.FC = () => {
    const [showAddItemForm, setShowAddItemForm] = useState(false);
    const [items, setItems] = useState<Item[]>([]);
    const [editedItems, setEditedItems] = useState<number[]>([]);
    const doneItems = useMemo(() => items.filter((item) => item.done).length, [items]);
    const todoItems = useMemo(() => items.filter((item) => !item.done).length, [items]);
    
    const handleAddItem = () => setShowAddItemForm(true);
    const handleCancel = () => setShowAddItemForm(false);
    
    const handleEditCancelFactory = (id: number) => () => setEditedItems(editedItems.filter((item) => item !== id));
    const handleEditSubmitFactory = (id: number) => async (value: string | number | undefined) => {
        handleEditCancelFactory(id)();
        await fetchApi(`/items/${id}`, METHODS.PATCH, { title: value });
        await fetchItems();
    };


    const fetchItems = async () => {
        const data: Item[] = await fetchApi("/items", METHODS.GET);
        const sortedData = sortItems(data);
        setItems(sortedData);
    };
    const handleSubmit = async (value: string | number | undefined) => {
        setShowAddItemForm(false);
        await fetchApi("/items", METHODS.POST, { title: value , done: false });
        await fetchItems();
    };
    const handleDeleteFactory = (id: number) => async () => {
        await fetchApi(`/items/${id}`, METHODS.DELETE);
        await fetchItems();
    };
    const handleCheckedFactory = (id: number) => async (value: boolean) => {
        await fetchApi(`/items/${id}/done`, METHODS.PATCH, { done: value });
        await fetchItems();
    };
    const handleEditFactory = (id: number) => () => setEditedItems([...editedItems, id]);

    useEffect(() => {
        fetchItems();
    }, []);



    return (
        <ThemeProvider>
            <Container>
                <Layout>
                    <Header handleAddItem={handleAddItem}>
                        {showAddItemForm && <Form handleCancel={handleCancel} handleSubmit={handleSubmit} initialValue=""/>}
                    </Header>
                    <List>
                        {items.map((item) => {
                            const isEdited = editedItems.includes(item.id);
                            return (
                                <ListItem 
                                    key={item.id} 
                                    label={
                                        isEdited ? 
                                            <Form 
                                                handleCancel={handleEditCancelFactory(item.id)} 
                                                handleSubmit={handleEditSubmitFactory(item.id)} 
                                                initialValue={item.title ?? ""}
                                            /> :
                                            item.title
                                    }
                                    handleEdit={handleEditFactory(item.id)} 
                                    handleRemoval={handleDeleteFactory(item.id)}
                                    onCheckedChange={handleCheckedFactory(item.id)}
                                    checked={item.done}
                                    disabled={isEdited}
                                />
                            );})}
                    </List>
                    <Footer todoItems={todoItems} doneItems={doneItems}/>
                </Layout>
            </Container>
        </ThemeProvider>
    );};
