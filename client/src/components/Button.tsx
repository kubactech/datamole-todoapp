import React from "react";
import styled from "styled-components";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    /**
     * icon to be rendered inside the button 
    */
    children: React.ReactNode;
     /**
     * callback on click
    */
    onClick?: () => void;
}

export const StyledButton = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    border: 1px solid;
    border-color: ${(props) => props.theme.colors.olive9};
    background-color: ${(props) => props.theme.colors.grass9};
    color: #fff;
    width: 25px;
    height: 25px;
`;

export const Button: React.FC<ButtonProps> = ({ children, onClick }) => {
    return (
        <StyledButton onClick={onClick}>
            {children}
        </StyledButton>
    );
};