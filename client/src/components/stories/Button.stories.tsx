import { Meta, StoryObj } from "@storybook/react";

import { PlusIcon,Pencil1Icon, TrashIcon,CheckIcon, Cross1Icon  } from "@radix-ui/react-icons";
import { Button } from "../Button";

const meta = {
    title: "Button",
    component: Button,
} as Meta<typeof Button>;
export default meta;

type Story = StoryObj<typeof Button>;

const argTypes = {
    onClick: { action: "clicked" },
};

export const Plus: Story = {
    args: {
        children: <PlusIcon/>,
    },
    ...argTypes
};

export const Pencil: Story = {
    args: {
        children: <Pencil1Icon/>,
    },
    ...argTypes
};

export const Trash: Story = {
    args: {
        children: <TrashIcon/>,
    },
    ...argTypes
};

export const Check: Story = {
    args: {
        children: <CheckIcon/>,
    },
    ...argTypes
};

export const Cross: Story = {
    args: {
        children: <Cross1Icon/>,
    },
    ...argTypes
};
