import { Meta, StoryObj } from "@storybook/react";

import { Layout } from "../Layout";

const meta = {
    title: "Layout",
    component: Layout,
} as Meta<typeof Layout>;

export default meta;

type Story = StoryObj<typeof Layout>;

export const Default: Story = {
    args: {
        children: 
            <div>
                <h1>This is layout</h1>
                <p>It is used to wrap the whole app</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.</p>
            </div>
        ,
    },
};