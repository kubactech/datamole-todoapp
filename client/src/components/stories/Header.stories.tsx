/* eslint-disable @typescript-eslint/no-empty-function */
import { Meta, StoryObj } from "@storybook/react";

import { Header } from "../Header";
import { Form } from "../form/Form";

const meta = {
    title: "Header",
    component: Header,
    argTypes: {
        handleAddItem: { action: "item added" },
    },
} as Meta<typeof Header>;
export default meta;
type Story = StoryObj<typeof Header>;


export const Default: Story = {
    args: {},
};

export const Clicked: Story = {
    args: {
        children: <Form initialValue="" handleCancel={()=>{}} handleSubmit={()=>{}}/>,
    },
};
