import React from "react";
import styled from "styled-components";
import { Checkbox } from "./Checkbox";
import { CheckboxProps } from "@radix-ui/react-checkbox";
import { Pencil1Icon, TrashIcon } from "@radix-ui/react-icons";
import { Button, StyledButton } from "./Button";

const StyledButtons = styled.div`
    display: none;
    align-items: center;
    margin-left: auto;
    ${StyledButton} {
        margin-left: 5px;
    }
    ${StyledButton}: first-child {
        margin-left: 15px;
    }
`;

const StyledDiv = styled.div<{hideButtons?:boolean}>`
    display: flex;
    align-items: center;
    margin-bottom: 15px;

    &:last-of-type {
        margin-bottom: 0;
    }

    &: hover {
        ${StyledButtons} {
            display: ${({ hideButtons }) => !hideButtons && "flex"};;
        }
    }
`;

const Label = styled.label`
    margin-left: 15px;
`;

export type LiteItemProp = CheckboxProps & {
    label: React.ReactNode;
    handleEdit: () => void;
    handleRemoval: () => void;
};

export const ListItem: React.FC<LiteItemProp> = ({ label, handleRemoval, handleEdit, ...checkboxProps }) => (
    <StyledDiv hideButtons={checkboxProps.disabled}>
        <Checkbox {...checkboxProps} />
        <Label>{label}</Label>
        <StyledButtons>
            <Button onClick={handleRemoval}>
                <TrashIcon />
            </Button>
            <Button onClick={handleEdit}>
                <Pencil1Icon />
            </Button>
        </StyledButtons>
    </StyledDiv>
);
