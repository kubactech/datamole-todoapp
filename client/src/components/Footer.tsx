import React from "react";
import styled from "styled-components";

const StyledDiv = styled.footer`
    display: flex;
    margin-top: auto;
    padding-top: 15px;
    border-top: 1px solid;
    border-color: ${(props) => props.theme.colors.olive6};
`;

const StyledSpan = styled.span`
    margin-right: 10px;
    span {
        font-weight: bold;
    }  
`;

export type FooterProps = {
    todoItems?: number;
    doneItems?: number;
};
export const Footer: React.FC<FooterProps> = ({ todoItems, doneItems }) => (
    <StyledDiv>
        <StyledSpan><span>Todo:</span> {todoItems || 0}</StyledSpan>
        <StyledSpan><span>Done:</span> {doneItems || 0}</StyledSpan>
    </StyledDiv>
);
