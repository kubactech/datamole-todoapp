import React from "react";
import styled from "styled-components";
import { PlusIcon } from "@radix-ui/react-icons";
import { Button } from "./Button";

export type HeaderProps = {
     /**
     * elements to be rendered instead of the default button
    */
    children: React.ReactNode;
    /**
     * function to be called when the default button is clicked
    */
    handleAddItem: () => void;
};

const StyledDiv = styled.header`
    display: flex;
    align-items: center;
    justify-content: flex-end;

    h1 {
        font-size: 1.5rem;
        font-weight: 500;
        margin-right: auto;
    }
`;

export const Header: React.FC<HeaderProps> = ({ handleAddItem, children }) => (
    <StyledDiv>
        <h1>Todo App</h1>
        {!children && 
        <Button onClick={handleAddItem}>
            <PlusIcon />
        </Button> }
        {children}
    </StyledDiv>
);
