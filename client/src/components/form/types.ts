export type FormProps = {
    handleSubmit: (data: string | number | undefined) => void;
    handleCancel: () => void;
    initialValue: string;
};

export type InputProps = Pick<FormProps, "initialValue"> & {
    handleInputChange: (value: string) => void;
};
