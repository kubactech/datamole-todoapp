import React, { useState } from "react";
import styled from "styled-components";
import { FormProps } from "./types";
import { Input } from "./Input";
import { CheckIcon, Cross1Icon } from "@radix-ui/react-icons";
import { Button, StyledButton } from "../Button";

const StyledForm = styled.form`
    display: flex;

    ${StyledButton}: first-of-type {
        margin-left: 15px;
        margin-right: 5px;
    }
`;

export const Form = (props: FormProps): JSX.Element => {
    const [data, setData] = useState<string>(props.initialValue);

    return (
        <StyledForm
            onSubmit={(e) => {
                e.preventDefault();
                let parsedData: number | string | undefined = data;
                if(data && !isNaN(+data)) {
                    parsedData = +data;
                } else if (data === "") {
                    parsedData = undefined;
                }
                props.handleSubmit(parsedData);
            }}
        >
            <Input initialValue={props.initialValue} handleInputChange={(value: string) => setData(value)} />
            <Button type={"submit"}>
                <CheckIcon />
            </Button>
            <Button onClick={props.handleCancel}>
                <Cross1Icon />
            </Button>
        </StyledForm>
    );
};
