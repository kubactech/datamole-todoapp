
export enum METHODS {
    GET = "GET",
    POST = "POST",
    PATCH = "PATCH",
    DELETE = "DELETE",
}

const FETCH_OPTIONS = {
    [ METHODS.POST ]: {
        method: METHODS.POST,
        headers: {
            "Content-Type": "application/json",
        },
    },
    [ METHODS.GET ]: {
        method: METHODS.GET,
    },
    [ METHODS.PATCH ]: {
        method: METHODS.PATCH,
        headers: {
            "Content-Type": "application/json",
        },
    },
    [ METHODS.DELETE ]: {
        method: METHODS.DELETE,
    },
};

const SERVER_URL = "http://localhost:3000";

const fetchApi = async (url: string, method: METHODS, body?: any) => {
    const options = FETCH_OPTIONS[ method ];
    const response = await fetch(`${SERVER_URL}${url}`, {
        ...options,
        body: JSON.stringify(body),
    });
    const data = await response.json();
    return data;
};

export default fetchApi;