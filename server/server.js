const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();

server.use(middlewares);

server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  if (req.method === "POST") {
    req.body.createdAt = Date.now();
  }
  if(req.path.includes("done")) {
    req.body.finishedAt = Date.now();
  }
  if(Object.keys(req.body).length === 0) {
    req.body.title= undefined
  }
  if(req.body?.done === false) {
    req.body.finishedAt= undefined
  }
  next();
});

// Use default router
server.use(jsonServer.rewriter({
  "/items/:id/done": "/items/:id"
}));
server.use(router);
server.listen(3000, () => {
  console.log("JSON Server is running");
});
